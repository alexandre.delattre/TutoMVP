package com.ibp.mvptutorial

import android.app.Application
import com.github.salomonbrys.kodein.*
import com.ibp.mvptutorial.weather.weatherActivityModule


/**
 * Created by Alexandre Delattre on 16/06/2017.
 */
open class WeatherApplication() : Application(), KodeinAware {
    override val kodein by Kodein.lazy {
        import(weatherActivityModule)
    }

}