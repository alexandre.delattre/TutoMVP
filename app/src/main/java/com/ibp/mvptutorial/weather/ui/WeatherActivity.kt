package com.ibp.mvptutorial.weather.ui

import android.content.Context
import android.support.design.widget.Snackbar
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.with
import com.ibp.mvptutorial.mvp.MvpActivity
import com.ibp.mvptutorial.R
import com.ibp.mvptutorial.addSimpleTextChangedListener
import com.ibp.mvptutorial.weather.models.WeatherData
import com.ibp.mvptutorial.weather.ui.WeatherContracts.WeatherPresenter
import com.ibp.mvptutorial.weather.ui.WeatherContracts.WeatherView
import com.squareup.picasso.Picasso


class WeatherActivity : MvpActivity<WeatherView>(), WeatherView {

    override val layout = R.layout.activity_main
    override val presenter by with(this).instance<WeatherPresenter>()

    override fun displayLoader(loading: Boolean) {
        TODO()
    }

    override fun enableSearchButton(enabled: Boolean) {
        TODO()
    }

    override fun displayWeatherLayout(visible: Boolean) {
        TODO()
    }

    override fun displayWeather(data: WeatherData) {
        TODO()
    }

    override fun displayError(e: Throwable) {
        TODO()
    }

    override fun initUi() {
        TODO()
    }
}

