package com.ibp.mvptutorial.weather.ui

import com.ibp.mvptutorial.mvp.MvpPresenter
import com.ibp.mvptutorial.mvp.MvpView

object WeatherContracts {
    interface WeatherView : MvpView {
        fun displayLoader(loading: Boolean)
        fun enableSearchButton(enabled: Boolean)
        fun displayWeatherLayout(visible: Boolean)
        fun displayWeather(data: com.ibp.mvptutorial.weather.models.WeatherData)
        fun displayError(e: Throwable)
    }

    interface WeatherPresenter : MvpPresenter<WeatherView> {
        var city: String
        fun searchWeather()
    }
}