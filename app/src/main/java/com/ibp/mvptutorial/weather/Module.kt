package com.ibp.mvptutorial.weather

import com.github.salomonbrys.kodein.Kodein.Module
import com.github.salomonbrys.kodein.android.androidActivityScope
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.scopedSingleton
import com.github.salomonbrys.kodein.singleton
import com.ibp.mvptutorial.weather.services.WeatherService
import com.ibp.mvptutorial.weather.services.WeatherServiceImpl
import com.ibp.mvptutorial.weather.services.geocode.GeocodeService
import com.ibp.mvptutorial.weather.services.geocode.GeocodeServiceImpl
import com.ibp.mvptutorial.weather.services.geocode.GoogleGeocodeApi
import com.ibp.mvptutorial.weather.services.weatherapi.WeatherApiService
import com.ibp.mvptutorial.weather.services.weatherapi.openweathermap.OpenWeatherMapApi
import com.ibp.mvptutorial.weather.services.weatherapi.openweathermap.OpenWeatherMapService
import com.ibp.mvptutorial.weather.ui.WeatherContracts.WeatherPresenter
import com.ibp.mvptutorial.weather.ui.WeatherPresenterImpl
import io.reactivex.android.schedulers.AndroidSchedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

/**
 * Created by Alexandre Delattre on 16/06/2017.
 */
val weatherActivityModule = Module {
    bind<WeatherPresenter>() with scopedSingleton(androidActivityScope) {
        WeatherPresenterImpl(instance(), AndroidSchedulers.mainThread())
    }


    bind<Retrofit>("gmaps") with singleton {
        val baseURL = "https://maps.googleapis.com"
        buildRetrofit(baseURL)
    }

    bind<Retrofit>("openweathermap") with singleton {
        buildRetrofit("http://api.openweathermap.org/data/2.5/")
    }

    bind<GoogleGeocodeApi>() with singleton {
        instance<Retrofit>("gmaps").create(GoogleGeocodeApi::class.java)
    }

    bind<GeocodeService>() with singleton {
        GeocodeServiceImpl("", instance())
    }

    bind<OpenWeatherMapApi>() with singleton {
        instance<Retrofit>("openweathermap").create(OpenWeatherMapApi::class.java)
    }

    bind<WeatherApiService>() with singleton {
        OpenWeatherMapService(instance(), "a936c94519e94feff4f84f095dc3fe76")
    }

    bind<WeatherService>() with singleton {
        WeatherServiceImpl(instance(), instance())
    }
}

fun buildRetrofit(baseURL: String): Retrofit {
    return Retrofit.Builder()
            .baseUrl(baseURL)
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
            .build()
}