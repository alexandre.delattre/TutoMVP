package com.ibp.mvptutorial.weather.services

import com.ibp.mvptutorial.weather.models.WeatherData
import io.reactivex.Single

interface WeatherService {

    fun getWeather(city: String): Single<WeatherData>
}


