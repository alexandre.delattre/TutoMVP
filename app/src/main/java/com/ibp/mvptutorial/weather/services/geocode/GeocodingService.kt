package com.ibp.mvptutorial.weather.services.geocode

import io.reactivex.Single

data class GeocodeResult(val city: String, val location: GoogleGeocodeApi.Location)
class CannotGeocodeException(place: String) : Exception("Cannot geocode $place")

interface GeocodeService {
    fun simpleGeocode(address: String): Single<GeocodeResult>
}



