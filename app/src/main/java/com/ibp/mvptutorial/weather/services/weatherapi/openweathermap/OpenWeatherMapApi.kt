package com.ibp.mvptutorial.weather.services.weatherapi.openweathermap

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

data class Main(val temp_min: Float,
                val temp_max: Float)

data class Weather(val icon: String)

data class OpenWeatherMapData(val main: Main, val name: String, val weather: List<Weather>)


interface OpenWeatherMapApi {

    @GET("weather")
    fun getWeather(@Query("APPID") appId: String, @Query("lat") latitude: Double, @Query("lon") longitude: Double,
                   @Query("units") units: String = "metric"): Single<OpenWeatherMapData>
}