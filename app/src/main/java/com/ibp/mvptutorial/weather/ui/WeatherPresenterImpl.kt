package com.ibp.mvptutorial.weather.ui

import com.ibp.mvptutorial.mvp.BasePresenter
import com.ibp.mvptutorial.weather.services.WeatherService
import com.ibp.mvptutorial.weather.ui.WeatherContracts.WeatherPresenter
import com.ibp.mvptutorial.weather.ui.WeatherContracts.WeatherView
import io.reactivex.Scheduler

class WeatherPresenterImpl(val weatherService: WeatherService, val scheduler: Scheduler)
    : BasePresenter<WeatherView>(), WeatherPresenter {

    private var loading = false
        set(value) {
            field = value
        }

    override fun create() {
        super.create()
    }

    override fun destroy() {
        super.destroy()
    }

    override var city: String = ""
        set(value) {
            field = value
        }

    override fun searchWeather() {
        TODO()
    }
}