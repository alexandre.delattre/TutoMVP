package com.ibp.mvptutorial.weather.services.weatherapi.openweathermap

import com.ibp.mvptutorial.weather.models.WeatherData
import com.ibp.mvptutorial.weather.services.weatherapi.WeatherApiService
import io.reactivex.Single

class OpenWeatherMapService(val api: OpenWeatherMapApi, val appId: String) : WeatherApiService {
    override fun getWeather(latitude: Double, longitude: Double): Single<WeatherData> {
        TODO()
    }
}


fun urlForOpenWeatherMapPicto(picto: String) = "http://openweathermap.org/img/w/$picto.png"