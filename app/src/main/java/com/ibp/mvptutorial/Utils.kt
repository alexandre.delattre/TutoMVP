package com.ibp.mvptutorial

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText

fun EditText.addSimpleTextChangedListener(fn: (String) -> Unit) {
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable) {
            fn(s.toString())
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }
    })
}