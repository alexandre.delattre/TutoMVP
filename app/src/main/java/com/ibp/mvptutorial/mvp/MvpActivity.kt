package com.ibp.mvptutorial.mvp

import android.os.Bundle
import com.github.salomonbrys.kodein.android.KodeinAppCompatActivity

/**
 * Created by itudenj on 08/06/2017.
 */
abstract class MvpActivity<V : MvpView> : KodeinAppCompatActivity(), MvpView {

    abstract val presenter: MvpPresenter<V>

    /**
     * Call after inflating and before call presenter.create()
     */
    abstract fun initUi()

    abstract val layout: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout)
        initUi()
        @Suppress("UNCHECKED_CAST")
        presenter.view = this as V
        presenter.create()
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun onStop() {
        super.onStop()
        presenter.stop()
    }

    override fun onDestroy() {
        presenter.destroy()
        presenter.view = null
        super.onDestroy()
    }
}