package com.ibp.mvptutorial.mvp

/**
 * The P part of Mvp
 * Created by itudenj on 10/05/2017.
 */
interface MvpPresenter<V : MvpView> {

    /**
     * The MvpView this presenter is attach to
     */
    var view: V?

    /**
     * Lifecycle
     */
    fun create()
    fun start()
    fun stop()
    fun destroy()
}