package com.ibp.mvptutorial.twitter

import com.squareup.moshi.Json
import io.reactivex.Single
import retrofit2.http.*

/**
 * Created by Alexandre Delattre on 28/06/2017.
 */
interface TwitterApi {

    @FormUrlEncoded
    @POST("/oauth2/token")
    fun oauthToken(@Header("Authorization") authorization: String,
                   @Field("grant_type") grantType: String = "client_credentials"): Single<OauthToken>

}

data class OauthToken(@Json(name = "token_type") val tokenType: String,
                      @Json(name = "access_token") val accessToken: String)