package com.ibp.mvptutorial.twitter

import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.singleton
import com.ibp.mvptutorial.weather.buildRetrofit
import retrofit2.Retrofit

/**
 * Created by Alexandre Delattre on 28/06/2017.
 */
val twitterModule = Kodein.Module {

    bind<Retrofit>("gmaps") with singleton {
        buildRetrofit("https://api.twitter.com/")
    }
}